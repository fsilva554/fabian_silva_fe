package utils

import (
	"net/http"
	"time"

	"github.com/gorilla/securecookie"
)

var hashKey = []byte("such-encryption")
var blockKey = []byte("aaaaaaaaaaaaaaaa")
var cookieHandler = securecookie.New(hashKey, blockKey)

func StoreTokenInCookie(token string, w http.ResponseWriter) {
	value := map[string]string{
		"token": token,
	}

	cookieHandler.MaxAge(3600)

	encoded, err := cookieHandler.Encode("auth", value)
	if err != nil {

	}

	cookie := &http.Cookie{
		Name:  "auth",
		Value: encoded,
		Path:  "/",
	}
	http.SetCookie(w, cookie)

}

func GetTokenFromCookie(r *http.Request) (string, error) {
	cookie, err := r.Cookie("auth")

	if err != nil {
		return "", err
	}

	value := make(map[string]string)
	err = cookieHandler.Decode("auth", cookie.Value, &value)

	if err != nil {
		return "", err
	}

	return value["token"], nil
}

func DestroyCookie(w http.ResponseWriter) {

	cookie := &http.Cookie{
		Name:    "auth",
		Value:   "",
		Path:    "/",
		Expires: time.Unix(1414414788, 1414414788000),
	}
	http.SetCookie(w, cookie)
}
