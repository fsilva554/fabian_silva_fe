package routes

import (
	"fsilva/api"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type ResetPasswordToken struct {
	Token string
}

func ResetPasswordRoute(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	token := r.URL.Query()["token"]

	if len(token) == 0 {
		log.Fatal("No token provided")
	}

	RenderTemplate(w, "reset_password", token[0])
}

func PerformResetPasswordRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	password := r.FormValue("password")
	token := r.FormValue("token")

	err := api.ResetPassword(token, password)

	if err != nil {
		log.Fatal(err)
	}

	http.Redirect(w, r, "/", http.StatusFound)
}
