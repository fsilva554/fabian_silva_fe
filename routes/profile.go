package routes

import (
	"fsilva/api"
	"fsilva/utils"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func ProfileRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	token, err := utils.GetTokenFromCookie(r)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
	}

	profile, err := api.GetProfile(token)

	if err != nil {
		log.Fatal(err)
	}

	RenderTemplate(w, "profile", profile)
}
