package routes

import (
	"context"
	"encoding/json"
	"fsilva/api"
	"fsilva/api/structs"
	"fsilva/thirdparty"
	"fsilva/utils"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/oauth2"
)

func PerformOAuthSignInRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	url := thirdparty.OAuthConfig.AuthCodeURL("1", oauth2.ApprovalForce,
		oauth2.AccessTypeOnline)
	http.Redirect(w, r, url, http.StatusFound)
}

func OAuthCallbackHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	ctx := context.Background()
	code := r.FormValue("code")

	token, err := thirdparty.OAuthConfig.Exchange(ctx, code)
	if err != nil {
		log.Fatal(err)
	}

	client := thirdparty.OAuthConfig.Client(oauth2.NoContext, token)
	email, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")

	if err != nil {
		log.Fatal(err)
	}

	defer email.Body.Close()
	data, _ := ioutil.ReadAll(email.Body)

	user := structs.User{}

	err = json.Unmarshal(data, &user)

	if err != nil {
		log.Fatal(err)
	}

	session, err := api.GoogleSignIn(user.Email)

	if err != nil {
		log.Fatal(err)
	}

	utils.StoreTokenInCookie(session.Token, w)

	http.Redirect(w, r, "/profile", http.StatusFound)
}
