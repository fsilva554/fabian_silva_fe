package routes

import (
	"log"
	"net/http"

	"fsilva/api"
	"fsilva/utils"

	"github.com/julienschmidt/httprouter"
)

func SignInRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	RenderTemplate(w, "sign_in", nil)
}

func PerformSignInRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	email := r.FormValue("email")
	password := r.FormValue("password")

	session, err := api.SignIn(email, password)

	if err != nil {
		log.Fatal(err)
	}

	utils.StoreTokenInCookie(session.Token, w)

	http.Redirect(w, r, "/profile", http.StatusFound)
}
