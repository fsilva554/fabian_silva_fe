package routes

import (
	"log"
	"net/http"

	"fsilva/api"
	"fsilva/utils"

	"github.com/julienschmidt/httprouter"
)

func SignUpRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	RenderTemplate(w, "sign_up", nil)
}

func PerformSignUpRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	email := r.FormValue("email")
	password := r.FormValue("password")

	session, err := api.SignUp(email, password)

	if err != nil {
		log.Fatal(err)
	}

	utils.StoreTokenInCookie(session.Token, w)

	http.Redirect(w, r, "/profile", http.StatusFound)
}
