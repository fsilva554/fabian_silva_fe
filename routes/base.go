package routes

import (
	"html/template"
	"net/http"
)

func RenderTemplate(w http.ResponseWriter, templateName string, data interface{}) {
	t, _ := template.ParseFiles("templates/layouts/layout.tmpl", "templates/"+templateName+".tmpl") // Parse template file
	t.ExecuteTemplate(w, "layout", data)
}
