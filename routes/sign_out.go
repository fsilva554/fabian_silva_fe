package routes

import (
	"log"
	"net/http"

	"fsilva/api"
	"fsilva/utils"

	"github.com/julienschmidt/httprouter"
)

func SignOutRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	token, err := utils.GetTokenFromCookie(r)

	if err != nil {
		log.Fatal(err)
	}

	api.SignOut(token)
	utils.DestroyCookie(w)

	http.Redirect(w, r, "/", http.StatusFound)

}
