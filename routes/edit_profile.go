package routes

import (
	"fsilva/api"
	"fsilva/utils"
	"html/template"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func EditProfileRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	token, err := utils.GetTokenFromCookie(r)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
	}

	profile, err := api.GetProfile(token)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
	}

	t, err := template.ParseFiles("templates/layouts/layout.tmpl", "templates/edit_profile.tmpl") // Parse template file

	if err != nil {
		log.Fatal(err)
	}

	t.ExecuteTemplate(w, "layout", profile)
}

func PerformEditProfileRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	token, err := utils.GetTokenFromCookie(r)

	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
	}

	email := r.FormValue("email")
	name := r.FormValue("name")
	address := r.FormValue("address")
	tel := r.FormValue("tel")

	err = api.UpdateProfile(token, email, name, address, tel)

	if err != nil {
		log.Fatal(err)
	}

	http.Redirect(w, r, "/profile", http.StatusFound)
}
