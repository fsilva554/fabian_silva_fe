package routes

import (
	"fsilva/api"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func ForgotPasswordRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	RenderTemplate(w, "forgot_password", nil)
}

func PerformForgotPasswordRoute(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()

	email := r.FormValue("email")

	err := api.RequestPasswordReset(email)

	if err != nil {
		log.Fatal(err)
	}

	http.Redirect(w, r, "/", http.StatusFound)
}
