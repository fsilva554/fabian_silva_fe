package thirdparty

import (
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var OAuthConfig *oauth2.Config

func init() {
	OAuthConfig = configureOAuthClient()
}

func configureOAuthClient() *oauth2.Config {
	redirectURL := os.Getenv("OAUTH2_CALLBACK")
	if redirectURL == "" {
		redirectURL = "http://fsilva-fe.herokuapp.com/oauth_callback"
	}
	return &oauth2.Config{
		ClientID:     "484636510274-v098cn6qkcaae2lsgm7d4sbsh5lvq5gf.apps.googleusercontent.com",
		ClientSecret: "U0PsV8feERxJAw5jcWl4mr74",
		RedirectURL:  redirectURL,
		Scopes:       []string{"email", "profile"},
		Endpoint:     google.Endpoint,
	}
}
