package api

import (
	"errors"
	"net/http"
)

func SignOut(token string) error {
	request_url := "http://fsilva-api.herokuapp.com/sign_out"

	client := &http.Client{}
	request, err := http.NewRequest("POST", request_url, nil)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("X-Auth", token)
	rsp, err := client.Do(request)

	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusOK {
		return errors.New("")
	}
	return nil
}
