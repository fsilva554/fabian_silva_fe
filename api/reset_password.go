package api

import (
	"bytes"
	"net/http"
	"net/url"
)

func ResetPassword(token, email string) error {
	request_url := "http://fsilva-api.herokuapp.com/reset_password"

	form := url.Values{
		"email": {email},
		"token": {token},
	}

	body := bytes.NewBufferString(form.Encode())
	_, err := http.Post(request_url, "application/x-www-form-urlencoded", body)

	if err != nil {
		return err
	}

	return nil
}
