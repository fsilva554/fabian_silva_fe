package structs

type Profile struct {
	Email   string
	Name    string
	Address string
	Tel     string
}
