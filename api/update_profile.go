package api

import (
	"bytes"
	"errors"
	"net/http"
	"net/url"
)

func UpdateProfile(token, email, name, address, tel string) error {
	request_url := "http://fsilva-api.herokuapp.com/profile"

	form := url.Values{
		"email":   {email},
		"name":    {name},
		"address": {address},
		"tel":     {tel},
	}

	client := &http.Client{}
	body := bytes.NewBufferString(form.Encode())
	request, err := http.NewRequest("PUT", request_url, body)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("X-Auth", token)
	rsp, err := client.Do(request)

	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusOK {
		return errors.New("")
	}

	return nil
}
