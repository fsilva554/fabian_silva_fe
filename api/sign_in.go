package api

import (
	"bytes"
	"encoding/json"
	"fsilva/api/structs"
	"io/ioutil"
	"net/http"
	"net/url"
)

func SignIn(email, password string) (structs.Session, error) {
	request_url := "http://fsilva-api.herokuapp.com/sign_in"

	resp := structs.Session{}

	form := url.Values{
		"email":    {email},
		"password": {password},
	}

	body := bytes.NewBufferString(form.Encode())
	rsp, err := http.Post(request_url, "application/x-www-form-urlencoded", body)

	if err != nil {
		return resp, err
	}

	defer rsp.Body.Close()

	body_byte, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return resp, err
	}

	err = json.Unmarshal(body_byte, &resp)
	if err != nil {
		return resp, err
	}

	return resp, nil
}
