package api

import (
	"bytes"
	"net/http"
	"net/url"
)

func RequestPasswordReset(email string) error {
	request_url := "http://fsilva-api.herokuapp.com/request_password_reset"

	form := url.Values{
		"email": {email},
	}

	body := bytes.NewBufferString(form.Encode())
	_, err := http.Post(request_url, "application/x-www-form-urlencoded", body)

	if err != nil {
		return err
	}

	return nil
}
