package api

import (
	"encoding/json"
	"errors"
	"fsilva/api/structs"
	"io/ioutil"
	"net/http"
)

func GetProfile(token string) (structs.Profile, error) {
	request_url := "http://fsilva-api.herokuapp.com/profile"

	profile := structs.Profile{}

	client := &http.Client{}
	request, err := http.NewRequest("GET", request_url, nil)
	request.Header.Set("X-Auth", token)
	rsp, err := client.Do(request)

	if err != nil {
		return profile, err
	}

	if rsp.StatusCode != http.StatusOK {
		return profile, errors.New("")
	}

	defer rsp.Body.Close()

	body_byte, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return profile, err
	}

	err = json.Unmarshal(body_byte, &profile)
	if err != nil {
		return profile, err
	}

	return profile, nil
}
