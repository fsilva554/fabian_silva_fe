package main

import (
	"log"
	"net/http"
	"os"

	"fsilva/routes"

	"github.com/julienschmidt/httprouter"
)

func main() {

	router := httprouter.New()
	port := os.Getenv("PORT")

	if port == "" {
		port = "8080"
	}

	router.GET("/", routes.SignInRoute)

	router.GET("/sign_up", routes.SignUpRoute)
	router.POST("/sign_up", routes.PerformSignUpRoute)

	router.POST("/sign_out", routes.SignOutRoute)

	router.POST("/sign_in", routes.PerformSignInRoute)
	router.GET("/oauth_sign_in", routes.PerformOAuthSignInRoute)
	router.GET("/oauth_callback", routes.OAuthCallbackHandler)

	router.GET("/profile", routes.ProfileRoute)
	router.GET("/profile/edit", routes.EditProfileRoute)
	router.POST("/profile/edit", routes.PerformEditProfileRoute)

	router.GET("/forgot_password", routes.ForgotPasswordRoute)
	router.POST("/forgot_password", routes.PerformForgotPasswordRoute)

	router.GET("/reset_password", routes.ResetPasswordRoute)
	router.POST("/reset_password", routes.PerformResetPasswordRoute)

	router.NotFound = http.FileServer(http.Dir("public"))

	log.Fatal(http.ListenAndServe(":"+port, router))
}
